#!/bin/sh

MINIKUBE_VER="v1.4.0"
KUBECTL_VER="v1.16.1"

cd "$(dirname "$0")"

MINIKUBE_URL="https://storage.googleapis.com/minikube/releases/$MINIKUBE_VER/minikube-linux-amd64"
KUBECTL_URL="https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VER/bin/linux/amd64/kubectl"

MINIKUBE_BIN="minikube-$MINIKUBE_VER"
KUBECTL_BIN="kubectl-$KUBECTL_VER"

# Install minikube
if [ ! -x "$MINIKUBE_BIN" ]; then
    echo "Downloading minikube $MINIKUBE_VER ..."
    curl --continue-at - -Lo "$MINIKUBE_BIN" "$MINIKUBE_URL"
else
    echo "minikube $MINIKUBE_VER already downloaded"
fi
chmod 755 "$MINIKUBE_BIN"
ln -sf "$MINIKUBE_BIN" minikube

# Install kubectl
if [ ! -x "$KUBECTL_BIN" ]; then
    echo "Downloading kubectl $KUBECTL_VER"
    curl --continue-at - -Lo "$KUBECTL_BIN" "$KUBECTL_URL"
else
    echo "kubectl $KUBECTL_VER already downloaded"
fi
chmod 755 "$KUBECTL_BIN"
ln -sf "$KUBECTL_BIN" kubectl
